This small script extracts tar.gz files on an SGE cluster. 
It's handy if there are a lot of files that needs to be extracted. 

clone the repo
~~~
git clone git@gitlab.com:mmb-umcu/extracting_tar_gz.git
~~~
Copy the bash file to your directory
~~~
cp extracting_tar_gz/extract.sh
~~~
Create a logs/ directory
~~~
mkdir -p logs
~~~
Called the script:

~~~
for i in $(ls *tar.gz)
  do  qsub zip.sh $i; 
done
~~~ 

This will submit a job for each file to extract
