#!/bin/bash

#extracts tar.gz files. 
#to be called as:
#for i in $(ls *tar.gz); do  qsub zip.sh $i; done
#folder logs/ needs to be made in the same file

#$ -S /bin/bash
#$ -cwd
#$ -o logs/
#$ -e logs/
#$ -M A.C.Schurch@umcutrecht.nl


tar -xzvf $1 
